

package com.raywenderlich.android.starsync.ui.mainscreen

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.raywenderlich.android.starsync.BuildConfig
import com.raywenderlich.android.starsync.R
import com.raywenderlich.android.starsync.contract.PresenterContract
import com.raywenderlich.android.starsync.contract.ViewContract
import com.raywenderlich.android.starsync.repository.DataRepository
import com.raywenderlich.android.starsync.repository.local.LocalRepo
import com.raywenderlich.android.starsync.repository.model.People
import com.raywenderlich.android.starsync.repository.remote.RemoteRepo
import com.raywenderlich.android.starsync.utils.hide
import com.raywenderlich.android.starsync.utils.show
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity(), ViewContract {

    private var peopleListAdapter = PeopleListAdapter()

    private var presenter: PresenterContract? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup recyclerView
        recyclerView.apply {
            hasFixedSize()
            layoutManager = LinearLayoutManager(this.context)
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
            adapter = peopleListAdapter
        }

        // Setup the repository
        val localRepo = LocalRepo(applicationContext)
        val remoteRepo = RemoteRepo(applicationContext)
        val repository = DataRepository(localRepo, remoteRepo)

        // Setup the presenter
        val presenter = MainActivityPresenter(this, repository)

        lifecycle.addObserver(presenter)

        // Setup FAB
        fab.setOnClickListener {
            // 1
            presenter?.getData()

            // 2
            // demonstrateUsingMainDispatcher()
        }
    }

    private fun demonstrateUsingMainDispatcher() {
        GlobalScope.launch(Dispatchers.Main) {

            // Runs on Main Thread
            var stringToShow = "Luke"
            prompt(stringToShow)

            // Runs on Background Thread
            withContext(Dispatchers.IO) {
                delay(5000)
                stringToShow = "Darth Vader"
            }

            // Runs on Main Thread
            prompt(stringToShow)
        }
    }

    override fun updateWithData(itemList: List<People>) {
        peopleListAdapter.loadItemList(itemList)
    }

    override fun showLoading() {
        recyclerView.hide()
        loadingProgress.show()
    }

    override fun hideLoading() {
        recyclerView.show()
        loadingProgress.hide()
    }

    override fun prompt(string: String?) {
        // Log to logcat
        if (BuildConfig.DEBUG) {
            Log.d(javaClass.simpleName, string ?: "-")
        }

        // Show a snackbar message
        Snackbar.make(rootLayout, string ?: "-", Snackbar.LENGTH_SHORT).show()
    }

}