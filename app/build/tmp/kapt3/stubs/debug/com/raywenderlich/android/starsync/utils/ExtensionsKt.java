package com.raywenderlich.android.starsync.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u00006\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u001a\n\u0010\t\u001a\u00020\n*\u00020\u000b\u001a\u001e\u0010\f\u001a\u00020\u000b*\u00020\r2\b\b\u0001\u0010\u000e\u001a\u00020\u00062\b\b\u0002\u0010\u000f\u001a\u00020\u0010\u001a\f\u0010\u0011\u001a\u00020\u0010*\u00020\u0012H\u0007\u001a\n\u0010\u0013\u001a\u00020\n*\u00020\u000b\"\u0019\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0003\u0010\u0004\u00a8\u0006\u0014"}, d2 = {"BACKGROUND_THREAD", "Ljava/util/concurrent/ExecutorService;", "kotlin.jvm.PlatformType", "getBACKGROUND_THREAD", "()Ljava/util/concurrent/ExecutorService;", "logCoroutineInfo", "", "msg", "", "hide", "", "Landroid/view/View;", "inflate", "Landroid/view/ViewGroup;", "layoutRes", "attachToRoot", "", "isOnline", "Landroid/content/Context;", "show", "app_debug"})
public final class ExtensionsKt {
    private static final java.util.concurrent.ExecutorService BACKGROUND_THREAD = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final android.view.View inflate(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup $this$inflate, @androidx.annotation.LayoutRes()
    int layoutRes, boolean attachToRoot) {
        return null;
    }
    
    @androidx.annotation.RequiresPermission(value = "android.permission.ACCESS_NETWORK_STATE")
    public static final boolean isOnline(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$isOnline) {
        return false;
    }
    
    public static final void show(@org.jetbrains.annotations.NotNull()
    android.view.View $this$show) {
    }
    
    public static final void hide(@org.jetbrains.annotations.NotNull()
    android.view.View $this$hide) {
    }
    
    public static final int logCoroutineInfo(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
        return 0;
    }
    
    public static final java.util.concurrent.ExecutorService getBACKGROUND_THREAD() {
        return null;
    }
}