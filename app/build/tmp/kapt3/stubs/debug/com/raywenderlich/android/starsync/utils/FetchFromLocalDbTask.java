package com.raywenderlich.android.starsync.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0001B\u0017\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ+\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0016\u0010\r\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00020\u000e\"\u0004\u0018\u00010\u0002H\u0014\u00a2\u0006\u0002\u0010\u000fJ\u0018\u0010\u0010\u001a\u00020\u00112\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u0014R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0013"}, d2 = {"Lcom/raywenderlich/android/starsync/utils/FetchFromLocalDbTask;", "Landroid/os/AsyncTask;", "Ljava/lang/Void;", "", "Lcom/raywenderlich/android/starsync/repository/model/People;", "repository", "Lcom/raywenderlich/android/starsync/contract/DataRepositoryContract;", "itemListCallback", "Lcom/raywenderlich/android/starsync/contract/ItemListCallback;", "(Lcom/raywenderlich/android/starsync/contract/DataRepositoryContract;Lcom/raywenderlich/android/starsync/contract/ItemListCallback;)V", "getRepository", "()Lcom/raywenderlich/android/starsync/contract/DataRepositoryContract;", "doInBackground", "params", "", "([Ljava/lang/Void;)Ljava/util/List;", "onPostExecute", "", "result", "app_debug"})
public final class FetchFromLocalDbTask extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.util.List<? extends com.raywenderlich.android.starsync.repository.model.People>> {
    @org.jetbrains.annotations.Nullable()
    private final com.raywenderlich.android.starsync.contract.DataRepositoryContract repository = null;
    private final com.raywenderlich.android.starsync.contract.ItemListCallback itemListCallback = null;
    
    @java.lang.Override()
    protected void onPostExecute(@org.jetbrains.annotations.Nullable()
    java.util.List<com.raywenderlich.android.starsync.repository.model.People> result) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected java.util.List<com.raywenderlich.android.starsync.repository.model.People> doInBackground(@org.jetbrains.annotations.NotNull()
    java.lang.Void... params) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.raywenderlich.android.starsync.contract.DataRepositoryContract getRepository() {
        return null;
    }
    
    public FetchFromLocalDbTask(@org.jetbrains.annotations.Nullable()
    com.raywenderlich.android.starsync.contract.DataRepositoryContract repository, @org.jetbrains.annotations.NotNull()
    com.raywenderlich.android.starsync.contract.ItemListCallback itemListCallback) {
        super();
    }
}