package com.raywenderlich.android.starsync.contract;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u00020\u00032\n\u0010\u0004\u001a\u00060\u0005j\u0002`\u0006H&J\u0018\u0010\u0007\u001a\u00020\u00032\u000e\u0010\b\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\tH&\u00a8\u0006\u000b"}, d2 = {"Lcom/raywenderlich/android/starsync/contract/ItemListCallback;", "", "onError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onSuccess", "itemList", "", "Lcom/raywenderlich/android/starsync/repository/model/People;", "app_debug"})
public abstract interface ItemListCallback {
    
    public abstract void onError(@org.jetbrains.annotations.NotNull()
    java.lang.Exception e);
    
    public abstract void onSuccess(@org.jetbrains.annotations.Nullable()
    java.util.List<com.raywenderlich.android.starsync.repository.model.People> itemList);
}