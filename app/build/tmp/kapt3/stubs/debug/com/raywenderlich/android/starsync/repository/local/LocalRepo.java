package com.raywenderlich.android.starsync.repository.local;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0016J\u0016\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0016R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/raywenderlich/android/starsync/repository/local/LocalRepo;", "Lcom/raywenderlich/android/starsync/contract/LocalRepositoryContract;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "localDatabase", "Lcom/raywenderlich/android/starsync/repository/local/PeopleDao;", "getData", "", "Lcom/raywenderlich/android/starsync/repository/model/People;", "saveData", "", "itemList", "app_debug"})
public final class LocalRepo implements com.raywenderlich.android.starsync.contract.LocalRepositoryContract {
    private final com.raywenderlich.android.starsync.repository.local.PeopleDao localDatabase = null;
    
    @java.lang.Override()
    public void saveData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.raywenderlich.android.starsync.repository.model.People> itemList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.util.List<com.raywenderlich.android.starsync.repository.model.People> getData() {
        return null;
    }
    
    public LocalRepo(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}