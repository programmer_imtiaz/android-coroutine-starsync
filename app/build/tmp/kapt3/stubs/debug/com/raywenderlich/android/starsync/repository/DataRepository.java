package com.raywenderlich.android.starsync.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fH\u0016J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\r0\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J\u0016\u0010\u0014\u001a\u00020\u000f2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\r0\fH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0016"}, d2 = {"Lcom/raywenderlich/android/starsync/repository/DataRepository;", "Lcom/raywenderlich/android/starsync/contract/DataRepositoryContract;", "localRepo", "Lcom/raywenderlich/android/starsync/repository/local/LocalRepo;", "remoteRepo", "Lcom/raywenderlich/android/starsync/repository/remote/RemoteRepo;", "(Lcom/raywenderlich/android/starsync/repository/local/LocalRepo;Lcom/raywenderlich/android/starsync/repository/remote/RemoteRepo;)V", "getLocalRepo", "()Lcom/raywenderlich/android/starsync/repository/local/LocalRepo;", "getRemoteRepo", "()Lcom/raywenderlich/android/starsync/repository/remote/RemoteRepo;", "getDataFromLocal", "", "Lcom/raywenderlich/android/starsync/repository/model/People;", "getDataFromRemoteUsingCallback", "", "itemListCallback", "Lcom/raywenderlich/android/starsync/contract/ItemListCallback;", "getDataFromRemoteUsingCoroutines", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "saveData", "itemList", "app_debug"})
public final class DataRepository implements com.raywenderlich.android.starsync.contract.DataRepositoryContract {
    @org.jetbrains.annotations.NotNull()
    private final com.raywenderlich.android.starsync.repository.local.LocalRepo localRepo = null;
    @org.jetbrains.annotations.NotNull()
    private final com.raywenderlich.android.starsync.repository.remote.RemoteRepo remoteRepo = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.util.List<com.raywenderlich.android.starsync.repository.model.People> getDataFromLocal() {
        return null;
    }
    
    @java.lang.Override()
    public void saveData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.raywenderlich.android.starsync.repository.model.People> itemList) {
    }
    
    @java.lang.Override()
    public void getDataFromRemoteUsingCallback(@org.jetbrains.annotations.NotNull()
    com.raywenderlich.android.starsync.contract.ItemListCallback itemListCallback) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getDataFromRemoteUsingCoroutines(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.raywenderlich.android.starsync.repository.model.People>> p0) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.raywenderlich.android.starsync.repository.local.LocalRepo getLocalRepo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.raywenderlich.android.starsync.repository.remote.RemoteRepo getRemoteRepo() {
        return null;
    }
    
    public DataRepository(@org.jetbrains.annotations.NotNull()
    com.raywenderlich.android.starsync.repository.local.LocalRepo localRepo, @org.jetbrains.annotations.NotNull()
    com.raywenderlich.android.starsync.repository.remote.RemoteRepo remoteRepo) {
        super();
    }
}