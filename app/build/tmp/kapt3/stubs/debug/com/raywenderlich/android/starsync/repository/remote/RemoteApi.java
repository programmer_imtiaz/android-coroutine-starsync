package com.raywenderlich.android.starsync.repository.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/raywenderlich/android/starsync/repository/remote/RemoteApi;", "", "()V", "BASE_URL", "", "PEOPLE_ROUTE", "starWarsService", "Lcom/raywenderlich/android/starsync/repository/remote/RetrofitService;", "getStarWarsService", "()Lcom/raywenderlich/android/starsync/repository/remote/RetrofitService;", "app_debug"})
public final class RemoteApi {
    private static final java.lang.String BASE_URL = "https://swapi.co/api/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PEOPLE_ROUTE = "people";
    @org.jetbrains.annotations.NotNull()
    private static final com.raywenderlich.android.starsync.repository.remote.RetrofitService starWarsService = null;
    public static final com.raywenderlich.android.starsync.repository.remote.RemoteApi INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.raywenderlich.android.starsync.repository.remote.RetrofitService getStarWarsService() {
        return null;
    }
    
    private RemoteApi() {
        super();
    }
}