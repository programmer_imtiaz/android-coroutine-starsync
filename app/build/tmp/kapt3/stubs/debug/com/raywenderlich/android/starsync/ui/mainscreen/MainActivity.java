package com.raywenderlich.android.starsync.ui.mainscreen;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\b\u001a\u00020\tH\u0002J\b\u0010\n\u001a\u00020\tH\u0016J\u0012\u0010\u000b\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0014J\u0012\u0010\u000e\u001a\u00020\t2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\b\u0010\u0011\u001a\u00020\tH\u0016J\u0016\u0010\u0012\u001a\u00020\t2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/raywenderlich/android/starsync/ui/mainscreen/MainActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/raywenderlich/android/starsync/contract/ViewContract;", "()V", "peopleListAdapter", "Lcom/raywenderlich/android/starsync/ui/mainscreen/PeopleListAdapter;", "presenter", "Lcom/raywenderlich/android/starsync/contract/PresenterContract;", "demonstrateUsingMainDispatcher", "", "hideLoading", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "prompt", "string", "", "showLoading", "updateWithData", "itemList", "", "Lcom/raywenderlich/android/starsync/repository/model/People;", "app_debug"})
public final class MainActivity extends androidx.appcompat.app.AppCompatActivity implements com.raywenderlich.android.starsync.contract.ViewContract {
    private com.raywenderlich.android.starsync.ui.mainscreen.PeopleListAdapter peopleListAdapter;
    private com.raywenderlich.android.starsync.contract.PresenterContract presenter;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void demonstrateUsingMainDispatcher() {
    }
    
    @java.lang.Override()
    public void updateWithData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.raywenderlich.android.starsync.repository.model.People> itemList) {
    }
    
    @java.lang.Override()
    public void showLoading() {
    }
    
    @java.lang.Override()
    public void hideLoading() {
    }
    
    @java.lang.Override()
    public void prompt(@org.jetbrains.annotations.Nullable()
    java.lang.String string) {
    }
    
    public MainActivity() {
        super();
    }
}