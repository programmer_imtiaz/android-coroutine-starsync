package com.raywenderlich.android.starsync.ui.mainscreen;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0019\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0019\u001a\u00020\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u001aH\u0002J\b\u0010\u001c\u001a\u00020\u001aH\u0002J\b\u0010\u001d\u001a\u00020\u001aH\u0002J\b\u0010\u001e\u001a\u00020\u001aH\u0016J\u0014\u0010\u001f\u001a\u00020\u001a2\n\u0010 \u001a\u00060!j\u0002`\"H\u0016J\u0010\u0010#\u001a\u00020\u001a2\u0006\u0010$\u001a\u00020%H\u0016J\u0010\u0010&\u001a\u00020\u001a2\u0006\u0010$\u001a\u00020%H\u0016J\u0016\u0010\'\u001a\u00020\u001a2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020*0)H\u0002J\u0016\u0010+\u001a\u00020\u001a2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020*0)H\u0002J \u0010,\u001a\u00020\u001a2\u000e\u0010(\u001a\n\u0012\u0004\u0012\u00020*\u0018\u00010)2\u0006\u0010-\u001a\u00020.H\u0016R\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018\u00a8\u0006/"}, d2 = {"Lcom/raywenderlich/android/starsync/ui/mainscreen/MainActivityPresenter;", "Lcom/raywenderlich/android/starsync/contract/PresenterContract;", "Lkotlinx/coroutines/CoroutineScope;", "Landroidx/lifecycle/DefaultLifecycleObserver;", "view", "Lcom/raywenderlich/android/starsync/contract/ViewContract;", "repository", "Lcom/raywenderlich/android/starsync/contract/DataRepositoryContract;", "(Lcom/raywenderlich/android/starsync/contract/ViewContract;Lcom/raywenderlich/android/starsync/contract/DataRepositoryContract;)V", "coroutineContext", "Lkotlin/coroutines/CoroutineContext;", "getCoroutineContext", "()Lkotlin/coroutines/CoroutineContext;", "coroutineJob", "Lkotlinx/coroutines/CompletableJob;", "processingUsing", "Lcom/raywenderlich/android/starsync/utils/ProcessUsing;", "getRepository", "()Lcom/raywenderlich/android/starsync/contract/DataRepositoryContract;", "setRepository", "(Lcom/raywenderlich/android/starsync/contract/DataRepositoryContract;)V", "getView", "()Lcom/raywenderlich/android/starsync/contract/ViewContract;", "setView", "(Lcom/raywenderlich/android/starsync/contract/ViewContract;)V", "cleanup", "", "fetchData", "fetchUsingBackgroundThreads", "fetchUsingCoroutines", "getData", "handleError", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onDestroy", "owner", "Landroidx/lifecycle/LifecycleOwner;", "onResume", "saveDataUsingBackgroundThreads", "itemList", "", "Lcom/raywenderlich/android/starsync/repository/model/People;", "saveDataUsingCoroutines", "updateData", "fromString", "", "app_debug"})
public final class MainActivityPresenter implements com.raywenderlich.android.starsync.contract.PresenterContract, kotlinx.coroutines.CoroutineScope, androidx.lifecycle.DefaultLifecycleObserver {
    private final com.raywenderlich.android.starsync.utils.ProcessUsing processingUsing = null;
    private final kotlinx.coroutines.CompletableJob coroutineJob = null;
    @org.jetbrains.annotations.Nullable()
    private com.raywenderlich.android.starsync.contract.ViewContract view;
    @org.jetbrains.annotations.Nullable()
    private com.raywenderlich.android.starsync.contract.DataRepositoryContract repository;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public kotlin.coroutines.CoroutineContext getCoroutineContext() {
        return null;
    }
    
    @java.lang.Override()
    public void cleanup() {
    }
    
    @java.lang.Override()
    public void handleError(@org.jetbrains.annotations.NotNull()
    java.lang.Exception e) {
    }
    
    @java.lang.Override()
    public void getData() {
    }
    
    private final void fetchData() {
    }
    
    private final void fetchUsingBackgroundThreads() {
    }
    
    private final void fetchUsingCoroutines() {
    }
    
    @java.lang.Override()
    public void updateData(@org.jetbrains.annotations.Nullable()
    java.util.List<com.raywenderlich.android.starsync.repository.model.People> itemList, @org.jetbrains.annotations.NotNull()
    java.lang.String fromString) {
    }
    
    private final void saveDataUsingCoroutines(java.util.List<com.raywenderlich.android.starsync.repository.model.People> itemList) {
    }
    
    private final void saveDataUsingBackgroundThreads(java.util.List<com.raywenderlich.android.starsync.repository.model.People> itemList) {
    }
    
    @java.lang.Override()
    public void onResume(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner owner) {
    }
    
    @java.lang.Override()
    public void onDestroy(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner owner) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.raywenderlich.android.starsync.contract.ViewContract getView() {
        return null;
    }
    
    public final void setView(@org.jetbrains.annotations.Nullable()
    com.raywenderlich.android.starsync.contract.ViewContract p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.raywenderlich.android.starsync.contract.DataRepositoryContract getRepository() {
        return null;
    }
    
    public final void setRepository(@org.jetbrains.annotations.Nullable()
    com.raywenderlich.android.starsync.contract.DataRepositoryContract p0) {
    }
    
    public MainActivityPresenter(@org.jetbrains.annotations.Nullable()
    com.raywenderlich.android.starsync.contract.ViewContract view, @org.jetbrains.annotations.Nullable()
    com.raywenderlich.android.starsync.contract.DataRepositoryContract repository) {
        super();
    }
}