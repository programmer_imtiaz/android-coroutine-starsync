package com.raywenderlich.android.starsync.contract;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\u0014\u0010\u0005\u001a\u00020\u00032\n\u0010\u0006\u001a\u00060\u0007j\u0002`\bH&J \u0010\t\u001a\u00020\u00032\u000e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000b2\u0006\u0010\r\u001a\u00020\u000eH&\u00a8\u0006\u000f"}, d2 = {"Lcom/raywenderlich/android/starsync/contract/PresenterContract;", "", "cleanup", "", "getData", "handleError", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "updateData", "itemList", "", "Lcom/raywenderlich/android/starsync/repository/model/People;", "fromString", "", "app_debug"})
public abstract interface PresenterContract {
    
    public abstract void cleanup();
    
    public abstract void getData();
    
    public abstract void updateData(@org.jetbrains.annotations.Nullable()
    java.util.List<com.raywenderlich.android.starsync.repository.model.People> itemList, @org.jetbrains.annotations.NotNull()
    java.lang.String fromString);
    
    public abstract void handleError(@org.jetbrains.annotations.NotNull()
    java.lang.Exception e);
}