package com.raywenderlich.android.starsync.repository.local;

import java.lang.System;

@androidx.room.Database(entities = {com.raywenderlich.android.starsync.repository.model.People.class}, version = 1, exportSchema = false)
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\'\u0018\u0000 \u00052\u00020\u0001:\u0001\u0005B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&\u00a8\u0006\u0006"}, d2 = {"Lcom/raywenderlich/android/starsync/repository/local/PeopleDatabase;", "Landroidx/room/RoomDatabase;", "()V", "peopleDao", "Lcom/raywenderlich/android/starsync/repository/local/PeopleDao;", "Companion", "app_debug"})
public abstract class PeopleDatabase extends androidx.room.RoomDatabase {
    private static com.raywenderlich.android.starsync.repository.local.PeopleDatabase INSTANCE;
    public static final com.raywenderlich.android.starsync.repository.local.PeopleDatabase.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.raywenderlich.android.starsync.repository.local.PeopleDao peopleDao();
    
    public PeopleDatabase() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0006\u0010\b\u001a\u00020\tR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/raywenderlich/android/starsync/repository/local/PeopleDatabase$Companion;", "", "()V", "INSTANCE", "Lcom/raywenderlich/android/starsync/repository/local/PeopleDatabase;", "destroyInstance", "", "getInstance", "context", "Landroid/content/Context;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.raywenderlich.android.starsync.repository.local.PeopleDatabase getInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        public final void destroyInstance() {
        }
        
        private Companion() {
            super();
        }
    }
}