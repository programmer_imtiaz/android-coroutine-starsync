package com.raywenderlich.android.starsync.repository.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lcom/raywenderlich/android/starsync/repository/remote/RemoteRepo;", "Lcom/raywenderlich/android/starsync/contract/RemoteRepositoryContract;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getContext", "()Landroid/content/Context;", "retrofitService", "Lcom/raywenderlich/android/starsync/repository/remote/RetrofitService;", "getDataUsingCallback", "", "itemListCallback", "Lcom/raywenderlich/android/starsync/contract/ItemListCallback;", "getDataUsingCoroutines", "", "Lcom/raywenderlich/android/starsync/repository/model/People;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class RemoteRepo implements com.raywenderlich.android.starsync.contract.RemoteRepositoryContract {
    private final com.raywenderlich.android.starsync.repository.remote.RetrofitService retrofitService = null;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getDataUsingCoroutines(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.raywenderlich.android.starsync.repository.model.People>> p0) {
        return null;
    }
    
    @java.lang.Override()
    public void getDataUsingCallback(@org.jetbrains.annotations.NotNull()
    com.raywenderlich.android.starsync.contract.ItemListCallback itemListCallback) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public RemoteRepo(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}