package com.raywenderlich.android.starsync.ui.mainscreen;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/raywenderlich/android/starsync/ui/mainscreen/PeopleViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bindFields", "", "item", "Lcom/raywenderlich/android/starsync/repository/model/People;", "app_debug"})
public final class PeopleViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
    
    public final void bindFields(@org.jetbrains.annotations.NotNull()
    com.raywenderlich.android.starsync.repository.model.People item) {
    }
    
    public PeopleViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.View itemView) {
        super(null);
    }
}