package com.raywenderlich.android.starsync.repository.local;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.raywenderlich.android.starsync.repository.model.People;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class PeopleDao_Impl implements PeopleDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfPeople;

  public PeopleDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfPeople = new EntityInsertionAdapter<People>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `People`(`name`,`height`,`mass`,`hair_color`,`skin_color`,`eye_color`,`gender`) VALUES (?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, People value) {
        if (value.getName() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getName());
        }
        if (value.getHeight() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getHeight());
        }
        if (value.getMass() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getMass());
        }
        if (value.getHair_color() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getHair_color());
        }
        if (value.getSkin_color() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getSkin_color());
        }
        if (value.getEye_color() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getEye_color());
        }
        if (value.getGender() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getGender());
        }
      }
    };
  }

  @Override
  public void insert(final People item) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfPeople.insert(item);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<People> getPeopleList() {
    final String _sql = "SELECT * from people";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false);
    try {
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
      final int _cursorIndexOfHeight = CursorUtil.getColumnIndexOrThrow(_cursor, "height");
      final int _cursorIndexOfMass = CursorUtil.getColumnIndexOrThrow(_cursor, "mass");
      final int _cursorIndexOfHairColor = CursorUtil.getColumnIndexOrThrow(_cursor, "hair_color");
      final int _cursorIndexOfSkinColor = CursorUtil.getColumnIndexOrThrow(_cursor, "skin_color");
      final int _cursorIndexOfEyeColor = CursorUtil.getColumnIndexOrThrow(_cursor, "eye_color");
      final int _cursorIndexOfGender = CursorUtil.getColumnIndexOrThrow(_cursor, "gender");
      final List<People> _result = new ArrayList<People>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final People _item;
        final String _tmpName;
        _tmpName = _cursor.getString(_cursorIndexOfName);
        final String _tmpHeight;
        _tmpHeight = _cursor.getString(_cursorIndexOfHeight);
        final String _tmpMass;
        _tmpMass = _cursor.getString(_cursorIndexOfMass);
        final String _tmpHair_color;
        _tmpHair_color = _cursor.getString(_cursorIndexOfHairColor);
        final String _tmpSkin_color;
        _tmpSkin_color = _cursor.getString(_cursorIndexOfSkinColor);
        final String _tmpEye_color;
        _tmpEye_color = _cursor.getString(_cursorIndexOfEyeColor);
        final String _tmpGender;
        _tmpGender = _cursor.getString(_cursorIndexOfGender);
        _item = new People(_tmpName,_tmpHeight,_tmpMass,_tmpHair_color,_tmpSkin_color,_tmpEye_color,_tmpGender);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
