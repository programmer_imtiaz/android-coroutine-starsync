1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.raywenderlich.android.starsync"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="19"
8-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="29" />
9-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.INTERNET" />
11-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:6:3-64
11-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:6:20-62
12    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
12-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:7:3-76
12-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:7:20-74
13    <uses-permission android:name="android.permission.WAKE_LOCK" />
13-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:25:5-68
13-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:25:22-65
14    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
14-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:27:5-81
14-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:27:22-78
15
16    <application
16-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:9:3-30:17
17        android:name="com.raywenderlich.android.starsync.StarSyncApp"
17-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:10:7-34
18        android:allowBackup="false"
18-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:11:7-34
19        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
19-->[androidx.core:core:1.0.1] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/ab9946b6f7a7a95ce5c79b7be619a883/core-1.0.1/AndroidManifest.xml:22:18-86
20        android:debuggable="true"
21        android:icon="@mipmap/ic_launcher"
21-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:12:7-41
22        android:label="@string/app_name"
22-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:13:7-39
23        android:roundIcon="@mipmap/ic_launcher_round"
23-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:14:7-52
24        android:supportsRtl="true"
24-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:15:7-33
25        android:testOnly="true"
26        android:theme="@style/AppTheme" >
26-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:16:7-38
27
28        <!-- Splash Activity -->
29        <activity android:name="com.raywenderlich.android.starsync.ui.splashscreen.SplashActivity" >
29-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:20:5-26:16
29-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:20:15-61
30            <intent-filter>
30-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:21:7-25:23
31                <action android:name="android.intent.action.MAIN" />
31-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:22:9-60
31-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:22:17-58
32
33                <category android:name="android.intent.category.LAUNCHER" />
33-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:24:9-68
33-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:24:19-66
34            </intent-filter>
35        </activity>
36
37        <!-- Main Activity -->
38        <activity android:name="com.raywenderlich.android.starsync.ui.mainscreen.MainActivity" />
38-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:29:5-59
38-->/Users/imtiaz/AndroidStudioProjects/StarSync/app/src/main/AndroidManifest.xml:29:15-57
39
40        <provider
40-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:30:9-36:35
41            android:name="androidx.work.impl.WorkManagerInitializer"
41-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:31:13-69
42            android:authorities="com.raywenderlich.android.starsync.workmanager-init"
42-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:32:13-68
43            android:directBootAware="false"
43-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:33:13-44
44            android:exported="false"
44-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:34:13-37
45            android:multiprocess="true" />
45-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:35:13-40
46
47        <service
47-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:38:9-43:35
48            android:name="androidx.work.impl.background.systemalarm.SystemAlarmService"
48-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:39:13-88
49            android:directBootAware="false"
49-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:40:13-44
50            android:enabled="@bool/enable_system_alarm_service_default"
50-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:41:13-72
51            android:exported="false" />
51-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:42:13-37
52        <service
52-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:44:9-50:35
53            android:name="androidx.work.impl.background.systemjob.SystemJobService"
53-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:45:13-84
54            android:directBootAware="false"
54-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:46:13-44
55            android:enabled="@bool/enable_system_job_service_default"
55-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:47:13-70
56            android:exported="true"
56-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:48:13-36
57            android:permission="android.permission.BIND_JOB_SERVICE" />
57-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:49:13-69
58
59        <receiver
59-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:52:9-57:35
60            android:name="androidx.work.impl.utils.ForceStopRunnable$BroadcastReceiver"
60-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:53:13-88
61            android:directBootAware="false"
61-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:54:13-44
62            android:enabled="true"
62-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:55:13-35
63            android:exported="false" />
63-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:56:13-37
64        <receiver
64-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:58:9-68:20
65            android:name="androidx.work.impl.background.systemalarm.ConstraintProxy$BatteryChargingProxy"
65-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:59:13-106
66            android:directBootAware="false"
66-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:60:13-44
67            android:enabled="false"
67-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:61:13-36
68            android:exported="false" >
68-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:62:13-37
69            <intent-filter>
69-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:64:13-67:29
70                <action android:name="android.intent.action.ACTION_POWER_CONNECTED" />
70-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:65:17-87
70-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:65:25-84
71                <action android:name="android.intent.action.ACTION_POWER_DISCONNECTED" />
71-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:66:17-90
71-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:66:25-87
72            </intent-filter>
73        </receiver>
74        <receiver
74-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:69:9-79:20
75            android:name="androidx.work.impl.background.systemalarm.ConstraintProxy$BatteryNotLowProxy"
75-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:70:13-104
76            android:directBootAware="false"
76-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:71:13-44
77            android:enabled="false"
77-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:72:13-36
78            android:exported="false" >
78-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:73:13-37
79            <intent-filter>
79-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:75:13-78:29
80                <action android:name="android.intent.action.BATTERY_OKAY" />
80-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:76:17-77
80-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:76:25-74
81                <action android:name="android.intent.action.BATTERY_LOW" />
81-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:77:17-76
81-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:77:25-73
82            </intent-filter>
83        </receiver>
84        <receiver
84-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:80:9-90:20
85            android:name="androidx.work.impl.background.systemalarm.ConstraintProxy$StorageNotLowProxy"
85-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:81:13-104
86            android:directBootAware="false"
86-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:82:13-44
87            android:enabled="false"
87-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:83:13-36
88            android:exported="false" >
88-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:84:13-37
89            <intent-filter>
89-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:86:13-89:29
90                <action android:name="android.intent.action.DEVICE_STORAGE_LOW" />
90-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:87:17-83
90-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:87:25-80
91                <action android:name="android.intent.action.DEVICE_STORAGE_OK" />
91-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:88:17-82
91-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:88:25-79
92            </intent-filter>
93        </receiver>
94        <receiver
94-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:91:9-100:20
95            android:name="androidx.work.impl.background.systemalarm.ConstraintProxy$NetworkStateProxy"
95-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:92:13-103
96            android:directBootAware="false"
96-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:93:13-44
97            android:enabled="false"
97-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:94:13-36
98            android:exported="false" >
98-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:95:13-37
99            <intent-filter>
99-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:97:13-99:29
100                <action android:name="android.net.conn.CONNECTIVITY_CHANGE" />
100-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:98:17-79
100-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:98:25-76
101            </intent-filter>
102        </receiver>
103        <receiver
103-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:101:9-112:20
104            android:name="androidx.work.impl.background.systemalarm.RescheduleReceiver"
104-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:102:13-88
105            android:directBootAware="false"
105-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:103:13-44
106            android:enabled="false"
106-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:104:13-36
107            android:exported="false" >
107-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:105:13-37
108            <intent-filter>
108-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:107:13-111:29
109                <action android:name="android.intent.action.BOOT_COMPLETED" />
109-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:108:17-79
109-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:108:25-76
110                <action android:name="android.intent.action.TIME_SET" />
110-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:109:17-73
110-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:109:25-70
111                <action android:name="android.intent.action.TIMEZONE_CHANGED" />
111-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:110:17-81
111-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:110:25-78
112            </intent-filter>
113        </receiver>
114        <receiver
114-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:113:9-122:20
115            android:name="androidx.work.impl.background.systemalarm.ConstraintProxyUpdateReceiver"
115-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:114:13-99
116            android:directBootAware="false"
116-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:115:13-44
117            android:enabled="@bool/enable_system_alarm_service_default"
117-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:116:13-72
118            android:exported="false" >
118-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:117:13-37
119            <intent-filter>
119-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:119:13-121:29
120                <action android:name="androidx.work.impl.background.systemalarm.UpdateProxies" />
120-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:120:17-98
120-->[androidx.work:work-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/3e6503735076ae8de0e110887b951a7e/work-runtime-2.1.0/AndroidManifest.xml:120:25-95
121            </intent-filter>
122        </receiver>
123
124        <service
124-->[androidx.room:room-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/5b2ae658b535ff5d7028032083f378e4/room-runtime-2.1.0/AndroidManifest.xml:25:9-27:40
125            android:name="androidx.room.MultiInstanceInvalidationService"
125-->[androidx.room:room-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/5b2ae658b535ff5d7028032083f378e4/room-runtime-2.1.0/AndroidManifest.xml:26:13-74
126            android:exported="false" />
126-->[androidx.room:room-runtime:2.1.0] /Users/imtiaz/.gradle/caches/transforms-2/files-2.1/5b2ae658b535ff5d7028032083f378e4/room-runtime-2.1.0/AndroidManifest.xml:27:13-37
127    </application>
128
129</manifest>
